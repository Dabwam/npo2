"use client";
import styles from "./page.module.css";
import Columns from "../../components/Columns";
import dynamic from "next/dynamic";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { API_URL, API_KEY } from "../../utils/urls";

export default function Home() {
  const MapWithNoSSR = dynamic(() => import("../../components/Map"), {
    ssr: false,
  });

  const range = "cameras-defb";
  const spreadsheetId = "1yeF2qCbgORvylGGO4WIhr3VY_WuLgHQObWCS0FiNTic";

  const [cameras, setCameras] = useState([]);

  useEffect(() => {
    axios
      .get(`${API_URL}/${spreadsheetId}/values/${range}?key=${API_KEY}`)
      .then((res) => {
        const tempCameras = res.data.values.map((cameraData) => {
          let cameraVariables = cameraData[0].split("-");
          let cameraNumber = parseInt(cameraVariables[2]);

          return {
            Number: cameraNumber,
            Name: cameraData[0],
            Longitude: parseFloat(cameraData[1]),
            Latitude: parseFloat(cameraData[2]),
          };
        });
        setCameras(tempCameras);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  if (!cameras || cameras.length === 0) {
    return <>Loading...</>;
  } else {
    return (
      <main className={styles.main}>
        <div id="map">
          <MapWithNoSSR cameras={cameras} />
        </div>
        <Columns cameras={cameras} />
      </main>
    );
  }
}
