"use client";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css";
import "leaflet-defaulticon-compatibility";

const Map = ({ cameras }) => {
  const validCameras = cameras.filter((camera) => !isNaN(camera.Longitude));

  const generateKey = (pre) => {
    return `${pre}_${new Date().getTime()}`;
  };

  return (
    <div>
      <MapContainer
        center={[52.093599, 5.118325]}
        zoom={9}
        scrollWheelZoom={false}
        id="mapid"
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {validCameras.map((camera, i) => (
          <Marker
            key={generateKey(i)}
            position={[camera.Longitude, camera.Latitude]}
          >
            <Popup>{camera.Name}</Popup>
          </Marker>
        ))}
      </MapContainer>
    </div>
  );
};

export default Map;
