import React from "react";

const Column = (columnId, cameras) => {
  const generateKey = (pre) => {
    return `${pre}_${new Date().getTime()}`;
  };

  if (!cameras || cameras.length === 0) return <></>;

  return (
    <table id={`column ${columnId}`}>
      <thead>
        <tr>
          <th colSpan="4">Cameras {columnId}</th>
        </tr>
        <tr>
          <th>Number</th>
          <th>Name</th>
          <th>Latitude</th>
          <th>Longitude</th>
        </tr>
      </thead>
      <tbody>
        {cameras &&
          cameras.map((camera, i) => (
            <tr key={generateKey(i)}>
              <td>{camera.Number}</td>
              <td>{camera.Name}</td>
              <td>{camera.Latitude}</td>
              <td>{camera.Longitude}</td>
            </tr>
          ))}
        <tr>
          <td></td>
        </tr>
      </tbody>
    </table>
  );
};

export default Column;
