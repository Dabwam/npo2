import React, { useEffect, useState } from "react";
import Column from "./Column.js";

const Columns = ({ cameras }) => {
  const [column1, setColumn1] = useState([]);
  const [column2, setColumn2] = useState([]);
  const [column3, setColumn3] = useState([]);
  const [column4, setColumn4] = useState([]);

  useEffect(() => {
    cameras.forEach((camera) => {
      sortCamera(camera);
    });
  }, [cameras]);

  const sortCamera = (camera) => {
    if (isNaN(camera.Number)) return;
    switch (true) {
      case camera.Number % 3 === 0 && camera.Number % 5 === 0:
        setColumn3((prevColumn3) => [...prevColumn3, camera]);
        break;
      case camera.Number % 3 === 0:
        setColumn1((prevColumn1) => [...prevColumn1, camera]);
        break;
      case camera.Number % 5 === 0:
        setColumn2((prevColumn2) => [...prevColumn2, camera]);
        break;
      default:
        setColumn4((prevColumn4) => [...prevColumn4, camera]);
    }
  };

  return (
    <>
      <div id="cameraTableContainer">
        {Column(1, column1)}
        {Column(2, column2)}
        {Column(3, column3)}
        {Column(4, column4)}
      </div>
    </>
  );
};

export default Columns;
